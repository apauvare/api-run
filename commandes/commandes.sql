drop table if exists commandes;

create table commandes (
    id integer primary key,
    nom varchar(25),
    description varchar(100)
);

insert into commandes values (0,'sudo', 'executer une commande en tant que superuser');
insert into commandes values (1,'man', 'afficher les informations d''une commande');
insert into commandes values (2,'ls', 'lister les fichiers dans le repertoire courant');
insert into commandes values (3,'cd', 'changer de repertoire');
insert into commandes values (4,'mkdir', 'creer un repertoire');
insert into commandes values (5,'cp', 'copier un fichier');
insert into commandes values (6,'rm', 'supprimer un fichier');
insert into commandes values (7,'grep', 'chercher et afficher des lignes qui correspondent a un pattern');
insert into commandes values (8,'cat', 'concatener et afficher le contenu de fichiers');
insert into commandes values (9,'pwd', 'afficher le repertoire courant');