drop table if exists conseils;

create table conseils (
    id integer primary key,
    nom varchar(100)
);

insert into conseils values (0, 'renforcer la securite');
insert into conseils values (1, 'utiliser la conteneurisation pour separer les services');
insert into conseils values (2, 'controler les droits d''acces');
insert into conseils values (3, 'adopter une solution verte');
insert into conseils values (4, 'bien connaitre le fonctionnement des serveurs et protocoles IP/TCP');
insert into conseils values (5, 'utiliser debian comme systeme d''exploitation');
insert into conseils values (6, 'avoir son propre nom de domaine');
insert into conseils values (7, 'relier son nom de domaine a son adresse ip');
insert into conseils values (8, 'rediriger les ports de la box vers son serveur');
insert into conseils values (9, 'configurer un access ssh securise');