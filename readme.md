# Bienvenue au projet API Run de Pauvarel Alexandre

## Important: avant d'utiliser le projet sur sa machine

 - Avoir Docker installe
 - Creer les docker networks suivants:
   - front_projet
   - backend_projet
   - backend_projet_2
   - backend_projet_3
   - backend_projet_4
   - backend_projet_5

## Liens du projet:
  - home.boomslang.picagraine.net
  - infos.boomslang.picagraine.net
  - commandes.boomslang.picagraine.net
  - conseils.boomslang.picagraine.net
  - ferrari.boomslang.picagraine.net
