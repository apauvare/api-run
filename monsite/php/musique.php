<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>École de musique</title>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	</head>
	
	<body>
	<h2>Liste des places disponibles par instrument</h2>
	<table border="1">
	<tr> <td><b>Instruments</b></td> <td><b>Places disponibles</b></td> </tr>
	
	<?php
	/* Nombre maximum d'élèves autorisé par instrument */
    $max_eleves = 20; 
    
    /* Nombre maximum d'élèves autorisé par instrument */
    $connexion = new PDO('pgsql:host=localhost;port=5432;dbname=alexpauv', 'alexpauv', 'iamnotAlex4210');	
    
    /** Préparation et exécution de la requête **/
    $sql = 'SELECT I.lib AS lib, :max - COUNT(E.inst) AS dispo 
            FROM Instrument I LEFT JOIN Eleve E ON E.inst=I.lib 
            GROUP BY lib';
    $resultset = $connexion->prepare($sql);
    $resultset->bindParam('max', $max_eleves);
    $resultset->execute();            
        
    while ($row = $resultset->fetch(PDO::FETCH_ASSOC)) {
        echo '<tr>';
        echo '<td>' . $row['lib'] . '</td>';
        echo '<td>' . $row['dispo'] . '</td>';
        echo '</tr>';
        }
        
    /** Déconnexion **/
    $connexion=null;    
    ?>
    
    </table>
    </body>
</html>