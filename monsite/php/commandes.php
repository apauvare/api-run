<?php
// Connexion à la base de données
$vConn = new PDO('pgsql:host=localhost;port=5432;dbname=alexpauv', 'alexpauv', 'iamnotAlex4210');

$req = 'select nom, description from commandes;';
$res = $vConn->query($req);

// Traitement du résultat
echo "<h1>10 commandes Linux importantes</h1>";
echo "<table border='1'>";
echo "<tr><th>Nom</th><th>Description</th></tr>";
while ($row = $res->fetch(PDO::FETCH_ASSOC)) {
	echo '<tr><td>'.$row['nom'].'</td><td>'.$row['description'].'</td></tr>';
}
echo "</table>";

// Lien de retour 
echo "<p><a href='../index.html'>Retour à l'accueil</a></p>";

// Clôture de la connexion
$vConn=null;
?>
