<?php 

/** Connexion **/
$connexion = new PDO('pgsql:host=localhost;port=5432;dbname=alexpauv', 'alexpauv', 'iamnotAlex4210');	

/** Préparation et exécution de la requête **/
$sql = "SELECT designation, prix FROM vfigurine";
$resultset = $connexion->prepare($sql);
$resultset->execute();

/** Traitement du résultat **/
echo "<catalogue>";
while ($row = $resultset->fetch(PDO::FETCH_ASSOC)) {
	echo "<figurine designation='" . $row['designation'] . "' prix='" . $row['prix'] . "'/>";
}
echo "</catalogue>";

/** Déconnexion **/
$connexion=null;

?>
