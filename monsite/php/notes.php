<?php
// Connexion à la base de données
$vConn = new PDO('pgsql:host=localhost;port=5432;dbname=alexpauv', 'alexpauv', 'iamnotAlex4210');

// Écriture, préparation et exécution de la requête 1
$vSql = 'SELECT login, nom, prenom FROM etudiant WHERE login=:login';
$vResultSet = $vConn->prepare($vSql);
$vResultSet->bindValue(':login',$_POST['login'],PDO::PARAM_STR);
$vResultSet->execute();
// Traitement du résultat
$vRow = $vResultSet->fetch(PDO::FETCH_ASSOC);
echo "<h1>Notes de l'étudiant $vRow[prenom] $vRow[nom] (<i>$vRow[login]</i>)</h1>";

// Écriture, préparation et exécution de la requête 2
$vSql = 'SELECT * FROM v_devoir WHERE etudiant=:login';
$vResultSet = $vConn->prepare($vSql);
$vResultSet->bindValue(':login',$_POST['login'],PDO::PARAM_STR);
$vResultSet->execute();
// Traitement du résultat
echo "<table border='1'>";
echo "<tr><th>Devoir</th><th>Date de rendu</th><th>Note</th></tr>";
while ($vRow = $vResultSet->fetch(PDO::FETCH_ASSOC)) {
	echo "<tr><td>$vRow[description]</td><td>$vRow[daterendu]</td><td>$vRow[valeur]</td></tr>";
}
echo "</table>";

// Écriture, préparation et exécution de la requête 3
$vSql = 'SELECT * FROM v_moy WHERE etudiant=:login';
$vResultSet = $vConn->prepare($vSql);
$vResultSet->bindValue(':login',$_POST['login'],PDO::PARAM_STR);
$vResultSet->execute();
// Traitement du résultat
$vRow = $vResultSet->fetch(PDO::FETCH_ASSOC);
echo "<p>Moyenne générale : <b>$vRow[moy]</b></p>";

// Lien de retour 
echo "<p><a href='../html/accueil.html'>Retour à l'accueil</a></p>";

// Clôture de la connexion
$vConn=null;
?>
