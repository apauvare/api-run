<!DOCTYPE html>
<html>
    <head>
        <title>Informations serveur</title>
        <meta charset="utf-8"/>
    </head>
  <body>
    <h1>Informations sur le serveur</h1>
    <?php
    $user = get_current_user();
    $dist_comm = 'grep "^PRETTY_NAME=" /etc/os-release | cut -d\= -f2';
    $dist = exec($dist_comm);
    trim($dist,'"');
    $kern = exec('uname -sr');

    $fh = fopen('/proc/meminfo','r');
    $ram_ava = 0;
    while ($line = fgets($fh)) {
        $pieces = array();
        if (preg_match('/^MemTotal:\s+(\d+)\skB$/', $line, $pieces)) {
            $ram_ava = $pieces[1];
            break;
        }
    }
    $ram_tot = 0;
    while ($line = fgets($fh)) {
        $pieces = array();
        if (preg_match('/^MemAvailable:\s+(\d+)\skB$/', $line, $pieces)) {
            $ram_tot = $pieces[1];
            break;
        }
    }
    fclose($fh);

    $disk_tot = disk_total_space("/");
    $disk_ava = disk_free_space("/");

    echo '<h2>Nom utilisateur</p><hr>';
    echo "<p>$user</p><br>";
    echo '<h2>Distribution</h2><hr>';
    echo "<p>$dist</p><br>";
    echo '<h2>Version du noyau Linux</h2><hr>';
    echo "<p>$kern</p><br>";
    echo '<h2>Taille memoire RAM</h2><hr>';
    echo "<p>Total: $ram_tot KB</p><br>";
    echo "<p>Disponible: $ram_ava KB</p><br>";
    echo '<h2>Taille du disque dur</h2><hr>';
    echo "<p>Total: $disk_tot KB</p><br>";
    echo "<p>Disponible: $disk_ava KB</p><br>";
	echo '<a href="../index.html">Retour</a>';
    ?>
  </body>
</html>
